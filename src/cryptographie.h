#ifndef CRYPTOGRAPHIE
#define CRYPTOGRAPHIE

char Chiffrement(char c, int cle);
char Dechiffrement(char c, int cle);
void AnalyseFreq(char* pszTexte, float jFreq [26]);
int CalculCle(float jFreq[26]);

#endif